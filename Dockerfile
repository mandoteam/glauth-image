FROM python:3-alpine as builder

# Setup work env
RUN mkdir /app
WORKDIR /app

ADD fetch_binaries.py .

# Find host arch and fetch respective binary
RUN python3 fetch_binaries.py

FROM debian:latest as runner

COPY --from=builder /app/glauth /usr/local/bin/glauth
RUN chmod +x /usr/local/bin/glauth

# Expose web and LDAP ports
EXPOSE 389 636 5555

CMD ["/usr/local/bin/glauth", "-c", "/etc/glauth/config.cfg"]
