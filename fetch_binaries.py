
import os
import subprocess
import stat

arch = os.uname().machine

print(arch)

if arch == 'x86_64' or arch == 'amd64':
    arch_suf = '64'
elif arch == 'x86_32' or arch == 'amd32':
    arch_suf = '32'
elif 'arm' in arch or 'aarch' in arch:
    if '64' in arch:
        arch_suf = '-arm64'
    elif '32' in arch:
        arch_suf = '-arm32'
    else:
        version = ''.join(filter(str.isdigit, arch))
        if int(version) > 7:
            arch_suf = '-arm64'
        else:
            arch_suf = '-arm32'

command = "wget https://github.com/glauth/glauth/releases/download/v1.1.2/glauth" + arch_suf + " -O /app/glauth"
subprocess.run(command.split(' '))

os.chmod("/app/glauth", stat.S_IXUSR)
os.chmod("/app/glauth", stat.S_IXGRP)
os.chmod("/app/glauth", stat.S_IXOTH)
