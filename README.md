# Glauth image

A multi-architecture docker image build for [Glauth](https://github.com/glauth/glauth).

Currently supporting linux/amd64 (**x86_64**), linux/arm64 and linux/arm/v7 (**arm32**).

Image available in the [Gitlab container registry](https://gitlab.com/mandoteam/glauth-image/container_registry).

## Usage

You should follow the instructions provided by the Glauth team. 

Additionally, you need to mount a config file in `/etc/glauth/config.cfg` with custom configurations. An examble is available [here](https://gitlab.com/mandoteam/glauth-image/-/blob/master/config/default-config.cfg).

Example usage (for testing purposes): 

```bash
docker run -p 5555:5555 -it -v $PWD/config/default-config.cfg:/etc/glauth/config.cfg registry.giltab.com/mandoteam/glauth-image:latest
```

